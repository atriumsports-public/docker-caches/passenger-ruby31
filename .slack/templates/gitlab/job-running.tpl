{
   "icon_emoji": ":gitlab:",
   "attachments": [
      {
         "fallback":"View <${CI_PROJECT_URL}/-/pipelines/${CI_PIPELINE_ID}|Pipeline progress>.",
         "pretext":":passenger_ship: Running job *${CI_JOB_NAME}* for *${CI_PROJECT_NAME}* from *${CI_COMMIT_REF_NAME}*",
         "color":"#3399FF",
         "fields":[
            {
               "title":"View Commit",
               "value":"<${CI_PROJECT_URL}/-/commit/${CI_COMMIT_SHA}|${CI_COMMIT_TITLE}>",
               "short":false
            },
            {
               "title":"View Logs",
               "value":"<${CI_PROJECT_URL}/-/jobs/${CI_JOB_ID}|Job #${CI_JOB_ID}>",
               "short":false
            },
             {
               "title":"View Progress",
               "value":"<${CI_PROJECT_URL}/-/pipelines/${CI_PIPELINE_ID}|Pipeline #${CI_PIPELINE_ID}>",
               "short":false
            }
         ]
      }
   ]
}
